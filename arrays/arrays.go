package arrays

import (
	"math"
)

// Chunk Chunk the array in as many pieces
func Chunk(array []interface{}, size int) [][]interface{} {
	if size == 0 {
		return nil
	}
	auxSize := int(math.Ceil(float64(len(array)) / float64(size)))
	arrResult := make([][]interface{}, auxSize)

	for key := 0; key < auxSize; key++ {
		indexKey := key * size
		for subKey := 0; subKey < size; subKey++ {
			auxKey := indexKey + subKey
			if auxKey >= len(array) {
				break
			}
			arrResult[key] = append(arrResult[key], array[auxKey])
		}
	}
	return arrResult
}

func Compact(array []interface{}) []interface{} {
	var result []interface{}

	for _, v := range array {
		if v == nil || v == false || v == 0 || v == "" {
			continue
		}
		result = append(result, v)
	}

	return result
}

func Difference(array []interface{}, values []interface{}) []interface{} {
	var result []interface{}
	for _, v := range array {
		if v == nil {
			continue
		}
		if Find(values, v) == -1 {
			result = append(result, v)
		}
	}
	return result
}

func Find(array []interface{}, value interface{}) int {
	for k, v := range array {
		if v == value {
			return k
		}
	}
	return -1
}

func FindLast(array []interface{}, value interface{}) int {
	index := -1
	for k, v := range array {
		if v == value {
			index = k
		}
	}
	return index
}
func FindAll(array []interface{}, value interface{}) []int {
	var index = make([]int, 0)
	for k, v := range array {
		if v == value {
			index = append(index, k)
		}
	}
	return index
}

func Intersection(array1 []interface{}, array2 []interface{}) []interface{} {
	result := make([]interface{}, 0)
	for _, value := range array1 {
		if value == nil {
			continue
		}
		if Find(array2, value) > -1 {
			result = append(result, value)
		}
	}
	return result
}

func Union(array1 []interface{}, array2 []interface{}) []interface{} {
	for _, value := range array2 {

		if Find(array1, value) > -1 {
			continue
		}
		array1 = append(array1, value)

	}
	return array1
}

func Uniq(array []interface{}) []interface{} {
	result := make([]interface{}, 0)
	for _, value := range array {
		if Find(result, value) > -1 {
			continue
		}
		result = append(result, value)
	}
	return result
}

func Zip(array ...[]interface{}) [][]interface{} {
	result := make([][]interface{}, len(array[0]))
	for _, value := range array {
		for key := 0; key < len(value); key++ {
			result[key] = append(result[key], value[key])
		}
	}
	return result
}

func Unzip(array [][]interface{}) [][]interface{} {
	result := make([][]interface{}, len(array[0]))
	for _, value := range array {
		for subKey := 0; subKey < len(value); subKey++ {
			result[subKey] = append(result[subKey], value[subKey])
		}
	}
	return result
}

func Without(array []interface{}, values ...interface{}) []interface{} {
	result := make([]interface{}, 1)
	for _, value := range array {
		if Find(values, value) == -1 {
			result = append(result, value)
		}
	}
	return result
}

func FlatIndex(array [][]interface{}, index int) []interface{} {
	result := make([]interface{}, len(array))
	for key, value := range array {
		result[key] = value[index]
	}
	return result
}
