package collection

import (
	"reflect"
	"sort"

	"gitlab.com/vinicius1889/godash/utils"

	"gitlab.com/vinicius1889/godash/arrays"
)

func Every(array []interface{}, function func(param interface{}) interface{}) []interface{} {
	result := make([]interface{}, len(array))
	for key, value := range array {
		result[key] = function(value)
	}
	return result
}

func Count(array []interface{}) map[interface{}]int {

	result := make(map[interface{}]int, 0)

	for _, value := range array {
		result[value] += 1
	}
	return result
}

func Filter(array []interface{}, filterFunc func(item interface{}) bool) []interface{} {
	result := make([]interface{}, 0)
	for _, value := range array {
		if filterFunc(value) {
			result = append(result, value)
		}
	}
	return result
}
func Reject(array []interface{}, filterFunc func(item interface{}) bool) []interface{} {
	result := make([]interface{}, 0)
	for _, value := range array {
		if !filterFunc(value) {
			result = append(result, value)
		}
	}
	return result
}

func Each(array []interface{}, function func(item interface{})) {
	for _, value := range array {
		function(value)
	}
}

func Group(array []interface{}) map[interface{}][]interface{} {
	result := make(map[interface{}][]interface{}, 0)
	for _, value := range array {
		result[value] = append(result[value], value)
	}
	return result
}

func OrderInt(array []interface{}, items []string, asc bool) []interface{} {
	tempArray := make([]interface{}, len(array))
	copy(tempArray, array)

	sort.Slice(tempArray, func(i, j int) bool {

		first := reflect.ValueOf(tempArray[i])
		second := reflect.ValueOf(tempArray[j])

		if !asc {
			first = reflect.ValueOf(tempArray[j])
			second = reflect.ValueOf(tempArray[i])
		}

		isGt := false

		for key, item := range items {
			if key > 0 {
				if first.FieldByName(items[key-1]).Int() == second.FieldByName(items[key-1]).Int() {
					isGt = first.FieldByName(items[key]).Int() < second.FieldByName(items[key]).Int()
				}
			} else {
				isGt = first.FieldByName(item).Int() < second.FieldByName(item).Int()
			}
		}
		return isGt

	})

	return tempArray
}

func Partition(array []interface{}, function func(item interface{}) bool) map[interface{}][]interface{} {
	result := make(map[interface{}][]interface{}, 2)
	for _, value := range array {
		key := "left"
		if !function(value) {
			key = "right"
		}
		result[key] = append(result[key], value)
	}
	return result
}

func ReduceValue(array map[interface{}]interface{}, function func(total interface{}, value interface{}) interface{}, initial interface{}) interface{} {
	for _, value := range array {
		initial = function(initial, value)
	}
	return initial
}

func Sample(array []interface{}, size int) []interface{} {
	if size > len(array) {
		return nil
	}
	result := make([]interface{}, 0)
	used := make([]interface{}, 0)

	myRand := utils.Rand()
	for i := 0; i < size; i++ {
		index := myRand.Intn(len(array))
		if arrays.Find(used, index) > -1 {
			i--
			continue
		}
		used = append(used, index)
		result = append(result, array[index])
	}
	return result
}

func Shuffle(array []interface{}) []interface{} {
	return Sample(array, len(array))
}

func CheckSome(array []interface{}) bool {
	for _, val := range array {
		if val.(bool) {
			return true
		}
	}
	return false
}
func CheckAll(array []interface{}) bool {

	for _, val := range array {
		if !val.(bool) {
			return false
		}
	}
	return true
}

func Pick(array []interface{}, props ...string) []map[string]interface{} {
	result := make([]map[string]interface{}, len(array))
	for key, value := range array {
		result[key] = make(map[string]interface{})
		for _, prop := range props {
			result[key][prop] = reflect.ValueOf(value).FieldByName(prop)
		}
	}
	return result
}

func PickValues(array []interface{}, props ...string) [][]reflect.Value {
	result := make([][]reflect.Value, len(array))
	for key, value := range array {
		result[key] = make([]reflect.Value, len(props))
		for subKey, prop := range props {
			result[key][subKey] = reflect.ValueOf(value).FieldByName(prop)
		}
	}
	return result
}
func PickInt(array []interface{}, props ...string) [][]int64 {
	result := make([][]int64, len(array))
	for key, value := range array {
		result[key] = make([]int64, len(props))
		for subKey, prop := range props {
			result[key][subKey] = reflect.ValueOf(value).FieldByName(prop).Int()
		}
	}
	return result
}
func PickString(array []interface{}, props ...string) [][]string {
	result := make([][]string, len(array))
	for key, value := range array {
		result[key] = make([]string, len(props))
		for subKey, prop := range props {
			result[key][subKey] = reflect.ValueOf(value).FieldByName(prop).String()
		}
	}
	return result
}
