package cond

type Conditional struct {
	item    interface{}
	valid   []bool
	isValid bool
}

func (c *Conditional) GT(num int) *Conditional {
	c.valid = append(c.valid, c.item.(int) > num)
	return c
}
func (c *Conditional) And() *Conditional {
	for _, v := range c.valid {
		c.isValid = (v && c.isValid)
	}
	return c
}

func (c *Conditional) IsTrue() bool {
	return c.isValid
}

func IF(item interface{}) *Conditional {
	return &Conditional{
		item:    item,
		isValid: true,
	}
}
