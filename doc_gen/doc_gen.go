package doc_gen

import (
	"io/ioutil"
	"strings"
)

func GenerateFromFile2(path string) {
	result := ""
	level := 0
	pre := false

	content, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	lines := string(content)
	for _, line := range strings.Split(lines, "\n") {
		if strings.Index(line, "/*") > -1 || strings.Index(line, "*/") > -1 {
			continue
		}

		if strings.Index(line, "<PRE>") > -1 {
			pre = true
			continue
		}
		if strings.Index(line, "</PRE>") > -1 {
			pre = false
			continue
		}

		if !pre {
			line = strings.TrimSpace(line)
		}

		if strings.Index(line, "<DOC>") > -1 {
			level++
			continue
		}
		if strings.Index(line, "</DOC>") > -1 {
			level--
			continue
		}

		if strings.Index(line, "<CODE>") > -1 {
			result += "\n```\n"
			continue
		}

		if strings.Index(line, "</CODE>") > -1 {
			result += "\n```\n"
			continue
		}

		if level > 0 {
			result += line
			result += "\n"
		}

	}
	ioutil.WriteFile(path+".md", []byte(result), 0644)
}
