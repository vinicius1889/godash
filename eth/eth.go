package eth

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	sha3 "golang.org/x/crypto/sha3"
)

func GetAddressWithout0x(address string) (string, error) {
	addr := address
	addr = string(regexp.MustCompile("0[xX]").ReplaceAll([]byte(addr), []byte("")))

	if regexp.MustCompile("[^0-9a-fA-F]").Match([]byte(addr)) {
		return "", fmt.Errorf("the address is not valid")
	}
	if len(addr) != 40 {
		return "", fmt.Errorf("the address length is not correct")
	}
	return addr, nil
}

func Keccak256(str string) string {
	keccak := sha3.NewLegacyKeccak256()
	keccak.Write([]byte(str))
	hash := keccak.Sum(nil)
	return fmt.Sprintf("%x", hash)
}

func Checksum(address string) (string, error) {
	addr, err := GetAddressWithout0x(address)
	if err != nil {
		return "", err
	}
	addr = strings.ToLower(addr)
	hash := strings.Split(Keccak256(addr), "")

	result := "0x"
	for k, v := range strings.Split(addr, "") {
		iV, _ := strconv.ParseInt(hash[k], 16, 8)
		if iV >= 8 {
			v = strings.ToUpper(v)
		}
		result += v
	}
	return result, nil
}
