module gitlab.com/vinicius1889/godash

go 1.13

require (
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71
	gopkg.in/go-playground/assert.v1 v1.2.1
)
