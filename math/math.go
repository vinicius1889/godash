package math

import (
	"fmt"
	"math"
	"strconv"
)

func Sum(items []interface{}) float64 {
	var result float64
	for _, v := range items {
		s := fmt.Sprintf("%v", v)
		parsed, _ := strconv.ParseFloat(s, 64)
		result += parsed
	}
	return result
}

func Max(items []interface{}) float64 {
	result := math.SmallestNonzeroFloat64
	for _, v := range items {
		s := fmt.Sprintf("%v", v)
		parsed, _ := strconv.ParseFloat(s, 64)
		if parsed > result {
			result = parsed
		}
	}
	return result
}

func Min(items []interface{}) float64 {
	result := math.MaxFloat64
	for _, v := range items {
		s := fmt.Sprintf("%v", v)
		parsed, _ := strconv.ParseFloat(s, 64)
		if parsed < result {
			result = parsed
		}
	}
	return result
}

func Mean(items []interface{}) float64 {
	result := math.SmallestNonzeroFloat64
	for _, v := range items {
		s := fmt.Sprintf("%v", v)
		parsed, _ := strconv.ParseFloat(s, 64)
		result += parsed
	}
	return result / float64(len(items))
}
