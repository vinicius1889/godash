package test

import (
	"testing"

	arrays "gitlab.com/vinicius1889/godash/arrays"

	"gopkg.in/go-playground/assert.v1"
)

/*
	<DOC>
	## Arrays package

	Import the module first

	<PRE>
	```
	import (
		"testing"
		arrays "gitlab.com/vinicius1889/godash/arrays"
		"gopkg.in/go-playground/assert.v1"
	)
	```
	</PRE>
	</DOC>
*/
func TestChunk(t *testing.T) {
	/*
		<DOC>
		### Chunk
		Creates an array of elements split into groups the length of size. If array can't be split evenly, the final chunk will be the remaining elements.
	*/

	// <CODE>
	// <PRE>
	arr := make([]interface{}, 4)
	arr[0] = 1
	arr[1] = 2
	arr[2] = 3
	arr[3] = 4
	result := arrays.Chunk(arr, 3)
	assert.Equal(t, len(result), 2)
	// </PRE>
	// </CODE>

	// </DOC>
}

func TestCompact(t *testing.T) {
	/*
		<DOC>
		### Compact
		Creates an array with all falsey values removed. The values false, null, 0, "".
		<CODE>
		<PRE>
	*/

	arr := make([]interface{}, 4)
	arr[0] = 0
	arr[1] = false
	arr[2] = ""
	arr[3] = "Joe"

	result := arrays.Compact(arr)
	assert.Equal(t, len(result), 1)

	/*
		</PRE>
		</CODE>
		</DOC>
	*/
}

func TestDifference(t *testing.T) {
	/*
		<DOC>
		### Difference
		Creates an array of array values not included in the other given arrays using SameValueZero for equality comparisons. The order and references of result values are determined by the first array.
		<CODE>
		<PRE>
	*/

	arr := make([]interface{}, 4)
	arr[0] = "godash"
	arr[1] = "here"
	arr[2] = "some"
	arr[3] = "stuff"

	values := make([]interface{}, 4)
	values[0] = "first"
	values[1] = "here"
	values[2] = "some"
	values[3] = "stuff"

	result := arrays.Difference(arr, values)
	assert.Equal(t, len(result), 1)

	/*
		</PRE>
		</CODE>
		</DOC>
	*/
}

func TestFindLast(t *testing.T) {
	/*
		<DOC>
		### FindLast
		This method return the last index of the passed interface
	*/

	// <CODE>
	// <PRE>
	arr := make([]interface{}, 4)
	arr[0] = "godash"
	arr[1] = "godash"
	arr[2] = "godash"
	arr[3] = "some"

	result := arrays.FindLast(arr, "godash")
	assert.Equal(t, result, 2)

	// </PRE>
	// </CODE>
	// </DOC>
}

func TestFindAll(t *testing.T) {
	/*
		<DOC>
		### FindAll
		This method return all indexes of the passed interface
	*/

	// <CODE>
	// <PRE>
	arr := make([]interface{}, 4)
	arr[0] = "godash"
	arr[1] = "godash"
	arr[2] = "godash"
	arr[3] = "some"

	result := arrays.FindAll(arr, "godash")
	assert.Equal(t, result, []int{0, 1, 2})
	// </PRE>
	// </CODE>
	// </DOC>
}

func TestIntersact(t *testing.T) {
	/*
		<DOC>
		### Intersection
		Creates an array of unique values that are included in all given arrays. The order and references of result values are determined by the first array.

	*/

	// <CODE>
	// <PRE>
	arr := make([]interface{}, 2)
	arr[0] = "godash"
	arr[1] = "nothing"

	values := make([]interface{}, 2)
	values[0] = "godash"
	values[1] = "nothing"

	result := arrays.Intersection(arr, values)
	assert.Equal(t, len(result), 2)
	// </PRE>
	// </CODE>
	// </DOC>
}

func TestUnion(t *testing.T) {
	/*
		<DOC>
		### Union
		Creates an array of unique values, in order, from all given arrays
	*/

	// <CODE>
	// <PRE>
	arr := make([]interface{}, 3)
	arr[0] = "item1"
	arr[1] = "item2"
	arr[2] = "item3"

	arr2 := make([]interface{}, 2)
	arr2[0] = "item4"
	arr2[1] = "item1"

	result := arrays.Union(arr, arr2)
	assert.Equal(t, len(result), 4)
	// </PRE>
	// </CODE>
	// </DOC>
}
func TestUniq(t *testing.T) {
	/*
		<DOC>
		### Uniq
		Creates a duplicate-free version of an array
	*/

	// <CODE>
	// <PRE>
	arr := make([]interface{}, 5)
	arr[0] = "item1"
	arr[1] = "item2"
	arr[2] = "item2"
	arr[3] = "item3"
	arr[4] = "item1"

	result := arrays.Uniq(arr)
	assert.Equal(t, len(result), 3)
	// </PRE>
	// </CODE>
	// </DOC>
}
func TestZip(t *testing.T) {
	/*
		<DOC>
		### Zip
		Creates an array of grouped elements, the first of which contains the first elements of the given arrays, the second of which contains the second elements of the given arrays, and so on	*/

	// <CODE>
	// <PRE>
	array := []interface{}{1, 2}
	array2 := []interface{}{3, 4}

	result := arrays.Zip(array, array2)
	assert.Equal(t, len(result), 2)
	// </PRE>
	// </CODE>
	// </DOC>
}
func TestUnzip(t *testing.T) {
	/*
		<DOC>
		### Unzip
		This method is like Zip except that it accepts an array of grouped elements and creates an array regrouping the elements to their pre-zip configuration.
	*/

	// <CODE>
	// <PRE>
	array := [][]interface{}{{"a", 1, true}, {"b", 2, false}}
	result := arrays.Unzip(array)
	assert.Equal(t, len(result), 3)
	// </PRE>
	// </CODE>
	// </DOC>
}

func TestWithout(t *testing.T) {
	/*
		<DOC>
		### Without

		Creates an array excluding all given values
	*/

	// <CODE>
	// <PRE>
	array := []interface{}{2, 1, 2, 3}
	result := arrays.Without(array, 1, 2)
	assert.Equal(t, arrays.Find(result, 1), -1)
	assert.Equal(t, arrays.Find(result, 2), -1)

	// </PRE>
	// </CODE>
	// </DOC>
}
