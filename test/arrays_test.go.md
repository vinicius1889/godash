## Arrays package

Import the module first

	```
	import (
		"testing"
		arrays "gitlab.com/vinicius1889/godash/arrays"
		"gopkg.in/go-playground/assert.v1"
	)
	```
### Chunk
Creates an array of elements split into groups the length of size. If array can't be split evenly, the final chunk will be the remaining elements.


```
	arr := make([]interface{}, 4)
	arr[0] = 1
	arr[1] = 2
	arr[2] = 3
	arr[3] = 4
	result := arrays.Chunk(arr, 3)
	assert.Equal(t, len(result), 2)

```

### Compact
Creates an array with all falsey values removed. The values false, null, 0, "".

```

	arr := make([]interface{}, 4)
	arr[0] = 0
	arr[1] = false
	arr[2] = ""
	arr[3] = "Joe"

	result := arrays.Compact(arr)
	assert.Equal(t, len(result), 1)


```
### Difference
Creates an array of array values not included in the other given arrays using SameValueZero for equality comparisons. The order and references of result values are determined by the first array.

```

	arr := make([]interface{}, 4)
	arr[0] = "godash"
	arr[1] = "here"
	arr[2] = "some"
	arr[3] = "stuff"

	values := make([]interface{}, 4)
	values[0] = "first"
	values[1] = "here"
	values[2] = "some"
	values[3] = "stuff"

	result := arrays.Difference(arr, values)
	assert.Equal(t, len(result), 1)


```
### FindLast
This method return the last index of the passed interface


```
	arr := make([]interface{}, 4)
	arr[0] = "godash"
	arr[1] = "godash"
	arr[2] = "godash"
	arr[3] = "some"

	result := arrays.FindLast(arr, "godash")
	assert.Equal(t, result, 2)


```
### FindAll
This method return all indexes of the passed interface


```
	arr := make([]interface{}, 4)
	arr[0] = "godash"
	arr[1] = "godash"
	arr[2] = "godash"
	arr[3] = "some"

	result := arrays.FindAll(arr, "godash")
	assert.Equal(t, result, []int{0, 1, 2})

```
### Intersection
Creates an array of unique values that are included in all given arrays. The order and references of result values are determined by the first array.



```
	arr := make([]interface{}, 2)
	arr[0] = "godash"
	arr[1] = "nothing"

	values := make([]interface{}, 2)
	values[0] = "godash"
	values[1] = "nothing"

	result := arrays.Intersection(arr, values)
	assert.Equal(t, len(result), 2)

```
### Union
Creates an array of unique values, in order, from all given arrays


```
	arr := make([]interface{}, 3)
	arr[0] = "item1"
	arr[1] = "item2"
	arr[2] = "item3"

	arr2 := make([]interface{}, 2)
	arr2[0] = "item4"
	arr2[1] = "item1"

	result := arrays.Union(arr, arr2)
	assert.Equal(t, len(result), 4)

```
### Uniq
Creates a duplicate-free version of an array


```
	arr := make([]interface{}, 5)
	arr[0] = "item1"
	arr[1] = "item2"
	arr[2] = "item2"
	arr[3] = "item3"
	arr[4] = "item1"

	result := arrays.Uniq(arr)
	assert.Equal(t, len(result), 3)

```
### Zip


```
	array := []interface{}{1, 2}
	array2 := []interface{}{3, 4}

	result := arrays.Zip(array, array2)
	assert.Equal(t, len(result), 2)

```
### Unzip
This method is like Zip except that it accepts an array of grouped elements and creates an array regrouping the elements to their pre-zip configuration.


```
	array := [][]interface{}{{"a", 1, true}, {"b", 2, false}}
	result := arrays.Unzip(array)
	assert.Equal(t, len(result), 3)

```
### Without

Creates an array excluding all given values


```
	array := []interface{}{2, 1, 2, 3}
	result := arrays.Without(array, 1, 2)
	assert.Equal(t, arrays.Find(result, 1), -1)
	assert.Equal(t, arrays.Find(result, 2), -1)


```
