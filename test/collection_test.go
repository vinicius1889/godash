package test

import (
	"testing"

	"gitlab.com/vinicius1889/godash/collection"

	"gopkg.in/go-playground/assert.v1"
)

/*
	<DOC>
	## Collection package

	Import the module first

	<PRE>

	import (
		"testing"

		"gitlab.com/vinicius1889/godash/collection"

		"gopkg.in/go-playground/assert.v1"
	)

	</PRE>
	</DOC>
*/

func TestEvery(t *testing.T) {
	/*
		<DOC>
		### Every
		Execute the function to each item. It returns a new array with transformed values
	*/

	// <CODE>
	// <PRE>
	arr := make([]interface{}, 2)
	arr[0] = 4
	arr[1] = 8

	function := func(item interface{}) interface{} {
		return 10 * item.(int)
	}
	result := collection.Every(arr, function)
	assert.Equal(t, result[0], 40)
	assert.Equal(t, result[1], 80)
	// </PRE>
	// </CODE>

	// </DOC>
}

func TestCount(t *testing.T) {
	/*
		<DOC>
		### Count
		Creates an object composed of keys generated from the results of running each element
	*/

	// <CODE>
	// <PRE>
	arr := make([]interface{}, 3)
	arr[0] = 4
	arr[1] = 4
	arr[2] = "5"

	result := collection.Count(arr)
	assert.Equal(t, result[4], 2)
	assert.Equal(t, result["5"], 1)
	// </PRE>
	// </CODE>

	// </DOC>
}

func TestFilter(t *testing.T) {
	/*
			<DOC>
			### Filter
		Iterates over elements of collection, returning an array of all elements predicate returns truthy for
	*/

	// <CODE>
	// <PRE>
	arr := make([]interface{}, 3)
	arr[0] = 1
	arr[1] = 2
	arr[2] = 3

	result := collection.Filter(arr, func(a interface{}) bool { return a.(int)%2 == 0 })
	assert.Equal(t, result[0], 2)

	// </PRE>
	// </CODE>

	// </DOC>
}
func TestReject(t *testing.T) {
	/*
			<DOC>
			### Reject
		Iterates over elements of collection, returning an array of all elements predicate returns falsy for
	*/

	// <CODE>
	// <PRE>
	arr := make([]interface{}, 3)
	arr[0] = 1
	arr[1] = 2
	arr[2] = 3

	result := collection.Reject(arr, func(a interface{}) bool { return a.(int)%2 == 0 })
	assert.Equal(t, result[0], 1)
	assert.Equal(t, result[1], 3)
	assert.Equal(t, len(result), 2)
	// </PRE>
	// </CODE>

	// </DOC>
}

func TestEach(t *testing.T) {
	arr := make([]interface{}, 3)
	arr[0] = 1
	arr[1] = 2
	arr[2] = 3

	function := func(a interface{}) {

	}
	collection.Each(arr, function)
}

func TestGroup(t *testing.T) {
	arr := make([]interface{}, 5)
	arr[0] = 1
	arr[1] = 2
	arr[2] = 3
	arr[3] = 3
	arr[4] = 3

	result := collection.Group(arr)
	assert.Equal(t, len(result[3]), 3)
}

func TestOrderInt(t *testing.T) {

	type Person struct {
		Name string
		Age  int
		Cod  int
		Cod2 int
	}

	item := make([]interface{}, 5)

	item[0] = Person{Name: "vinicius_0", Age: 1000, Cod: 120, Cod2: 110}
	item[1] = Person{Name: "vinicius_1", Age: 999, Cod: 99, Cod2: 99}
	item[2] = Person{Name: "vinicius_2", Age: 999, Cod: 98, Cod2: 70}
	item[3] = Person{Name: "vinicius_3", Age: 999, Cod: 97, Cod2: 500}
	item[4] = Person{Name: "vinicius_4", Age: 30, Cod: 1000, Cod2: 14}

	ordered := collection.OrderInt(item, []string{"Age", "Cod", "Cod2"}, true)
	assert.Equal(t, ordered[0].(Person).Name, "vinicius_4")
	assert.Equal(t, item[0].(Person).Name, "vinicius_0")
}
func TestOrderIntDesc(t *testing.T) {

	type Person struct {
		Name string
		Age  int
		Cod  int
		Cod2 int
	}

	item := make([]interface{}, 5)

	item[0] = Person{Name: "vinicius_0", Age: 1000, Cod: 120, Cod2: 110}
	item[1] = Person{Name: "vinicius_1", Age: 999, Cod: 99, Cod2: 98}
	item[2] = Person{Name: "vinicius_2", Age: 999, Cod: 99, Cod2: 99}
	item[3] = Person{Name: "vinicius_3", Age: 999, Cod: 98, Cod2: 500}
	item[4] = Person{Name: "vinicius_4", Age: 1000, Cod: 1000, Cod2: 14}

	ordered := collection.OrderInt(item, []string{"Age", "Cod", "Cod2"}, false)
	assert.Equal(t, ordered[0].(Person).Name, "vinicius_4")
	assert.Equal(t, item[0].(Person).Name, "vinicius_0")
}

func TestPartition(t *testing.T) {
	type Person struct {
		Name string
		Age  int
	}

	item := make([]interface{}, 3)

	item[0] = Person{Name: "vinicius_0", Age: 30}
	item[1] = Person{Name: "vinicius_1", Age: 60}
	item[2] = Person{Name: "vinicius_2", Age: 29}

	function := func(i interface{}) bool {
		return i.(Person).Age <= 30
	}
	parties := collection.Partition(item, function)
	assert.Equal(t, len(parties["left"]), 2)
}

func TestReduceValue(t *testing.T) {

	function := func(total interface{}, value interface{}) interface{} {
		t := total.(int)
		t += value.(int)
		return t
	}

	mapa := make(map[interface{}]interface{}, 0)
	mapa["a"] = 10
	mapa["b"] = 10
	mapa["c"] = 10
	mapa["d"] = 10
	mapa["e"] = 10

	sum := collection.ReduceValue(mapa, function, 0)
	assert.Equal(t, sum, 50)
}

func TestSample(t *testing.T) {
	item := make([]interface{}, 5)

	type Person struct {
		Name string
		Age  int
		Cod  int
		Cod2 int
	}

	item[0] = Person{Name: "vinicius_0", Age: 1000, Cod: 120, Cod2: 110}
	item[1] = Person{Name: "vinicius_1", Age: 999, Cod: 99, Cod2: 98}
	item[2] = Person{Name: "vinicius_2", Age: 999, Cod: 99, Cod2: 99}
	item[3] = Person{Name: "vinicius_3", Age: 999, Cod: 98, Cod2: 500}
	item[4] = Person{Name: "vinicius_4", Age: 1000, Cod: 1000, Cod2: 14}

	collection.Sample(item, 5)
}
func TestShuffle(t *testing.T) {
	item := make([]interface{}, 5)

	type Person struct {
		Name string
		Age  int
		Cod  int
		Cod2 int
	}

	item[0] = Person{Name: "vinicius_0", Age: 1000, Cod: 120, Cod2: 110}
	item[1] = Person{Name: "vinicius_1", Age: 999, Cod: 99, Cod2: 98}
	item[2] = Person{Name: "vinicius_2", Age: 999, Cod: 99, Cod2: 99}
	item[3] = Person{Name: "vinicius_3", Age: 999, Cod: 98, Cod2: 500}
	item[4] = Person{Name: "vinicius_4", Age: 1000, Cod: 1000, Cod2: 14}

	collection.Shuffle(item)
}
func TestCheckSome(t *testing.T) {
	item := make([]interface{}, 5)
	item[0] = false
	item[1] = false
	item[2] = false
	item[3] = false
	item[4] = false

	isValid := collection.CheckSome(item)
	assert.Equal(t, isValid, false)

	//true
	item[1] = true

	isValid = collection.CheckSome(item)
	assert.Equal(t, isValid, true)

}
func TestCheckAll(t *testing.T) {
	item := make([]interface{}, 5)
	item[0] = false
	item[1] = false
	item[2] = false
	item[3] = false
	item[4] = false

	isValid := collection.CheckAll(item)
	assert.Equal(t, isValid, false)

	item[0] = true
	item[1] = true
	item[2] = true
	item[3] = true
	item[4] = true

	isValid = collection.CheckAll(item)
	assert.Equal(t, isValid, true)

	item[0] = true
	item[1] = true
	item[2] = false
	item[3] = true
	item[4] = true

	isValid = collection.CheckAll(item)
	assert.Equal(t, isValid, false)

}

func TestPick(t *testing.T) {
	item := make([]interface{}, 5)

	type Person struct {
		Name string
		Age  int
		Cod  int
		Cod2 int
	}

	item[0] = Person{Name: "vinicius_0", Age: 1000, Cod: 120, Cod2: 110}
	item[1] = Person{Name: "vinicius_1", Age: 999, Cod: 99, Cod2: 98}
	item[2] = Person{Name: "vinicius_2", Age: 999, Cod: 99, Cod2: 99}
	item[3] = Person{Name: "vinicius_3", Age: 999, Cod: 98, Cod2: 500}
	item[4] = Person{Name: "vinicius_4", Age: 1000, Cod: 1000, Cod2: 14}
	collection.Pick(item, "Age")
}
func TestPickValues(t *testing.T) {
	item := make([]interface{}, 5)

	type Person struct {
		Name string
		Age  int
		Cod  int
		Cod2 int
	}

	item[0] = Person{Name: "vinicius_0", Age: 1000, Cod: 120, Cod2: 110}
	item[1] = Person{Name: "vinicius_1", Age: 999, Cod: 99, Cod2: 98}
	item[2] = Person{Name: "vinicius_2", Age: 999, Cod: 99, Cod2: 99}
	item[3] = Person{Name: "vinicius_3", Age: 999, Cod: 98, Cod2: 500}
	item[4] = Person{Name: "vinicius_4", Age: 1000, Cod: 1000, Cod2: 14}
	result := collection.PickValues(item, "Age")
	assert.Equal(t, result[0][0].Int(), int64(item[0].(Person).Age))
}
func TestPickInt(t *testing.T) {
	item := make([]interface{}, 5)

	type Person struct {
		Name string
		Age  int
		Cod  int
		Cod2 int
	}

	item[0] = Person{Name: "vinicius_0", Age: 1000, Cod: 120, Cod2: 110}
	item[1] = Person{Name: "vinicius_1", Age: 999, Cod: 99, Cod2: 98}
	item[2] = Person{Name: "vinicius_2", Age: 999, Cod: 99, Cod2: 99}
	item[3] = Person{Name: "vinicius_3", Age: 999, Cod: 98, Cod2: 500}
	item[4] = Person{Name: "vinicius_4", Age: 1000, Cod: 1000, Cod2: 14}
	result := collection.PickInt(item, "Age")
	assert.Equal(t, result[0][0], int64(1000))

}
func TestPickString(t *testing.T) {
	item := make([]interface{}, 3)

	type Person struct {
		Name string
		Age  int
		Cod  int
		Cod2 int
	}

	item[0] = Person{Name: "vinicius_1", Age: 10, Cod: 100, Cod2: 1000}
	item[1] = Person{Name: "vinicius_2", Age: 20, Cod: 200, Cod2: 2000}
	item[2] = Person{Name: "vinicius_3", Age: 30, Cod: 300, Cod2: 3000}
	result := collection.PickString(item, "Name")
	assert.Equal(t, result[0][0], "vinicius_1")
	assert.Equal(t, "vinicius_1", "vinicius_1")
}
