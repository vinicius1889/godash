## Collection package

Import the module first


	import (
		"testing"

		"gitlab.com/vinicius1889/godash/collection"

		"gopkg.in/go-playground/assert.v1"
	)

### Every
Execute the function to each item. It returns a new array with transformed values


```
	arr := make([]interface{}, 2)
	arr[0] = 4
	arr[1] = 8

	function := func(item interface{}) interface{} {
		return 10 * item.(int)
	}
	result := collection.Every(arr, function)
	assert.Equal(t, result[0], 40)
	assert.Equal(t, result[1], 80)

```

### Count
Creates an object composed of keys generated from the results of running each element


```
	arr := make([]interface{}, 3)
	arr[0] = 4
	arr[1] = 4
	arr[2] = "5"

	result := collection.Count(arr)
	assert.Equal(t, result[4], 2)
	assert.Equal(t, result["5"], 1)

```

### Filter
Iterates over elements of collection, returning an array of all elements predicate returns truthy for


```
	arr := make([]interface{}, 3)
	arr[0] = 1
	arr[1] = 2
	arr[2] = 3

	result := collection.Filter(arr, func(a interface{}) bool { return a.(int)%2 == 0 })
	assert.Equal(t, result[0], 2)


```

### Reject
Iterates over elements of collection, returning an array of all elements predicate returns falsy for


```
	arr := make([]interface{}, 3)
	arr[0] = 1
	arr[1] = 2
	arr[2] = 3

	result := collection.Reject(arr, func(a interface{}) bool { return a.(int)%2 == 0 })
	assert.Equal(t, result[0], 1)
	assert.Equal(t, result[1], 3)
	assert.Equal(t, len(result), 2)

```

