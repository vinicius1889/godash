package test

import (
	"testing"

	"gitlab.com/vinicius1889/godash/cond"
)

func TestIF(t *testing.T) {
	var item interface{}
	item = 10
	cond.IF(item).GT(100).And().IsTrue()

}
