package test

import (
	"io/ioutil"
	"strings"
	"testing"

	"gitlab.com/vinicius1889/godash/doc_gen"
)

func TestDocGenerate(t *testing.T) {
	path := "./utils_test.go"
	doc_gen.GenerateFromFile2(path)
}

func TestDocGenerateByDirect(t *testing.T) {
	path := "./"
	files, _ := ioutil.ReadDir(path)

	for _, v := range files {
		if strings.Index(v.Name(), ".md") > -1 {
			continue
		}
		doc_gen.GenerateFromFile2(v.Name())
	}
}
