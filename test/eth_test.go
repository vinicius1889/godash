package test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/vinicius1889/godash/eth"
)

func TestChecksum(t *testing.T) {
	result, _ := eth.Checksum("0x9258da398C416f5e6434C6Ce2b5164657a26a6eC")
	assert.Equal(t, "0x9258da398C416f5e6434C6Ce2b5164657a26a6eC", result)

	result, _ = eth.Checksum(strings.ToUpper("0x5D48aF8Ae8708d327d39481B0F397Eb260A069c4"))
	assert.Equal(t, "0x5D48aF8Ae8708d327d39481B0F397Eb260A069c4", result)

	result, _ = eth.Checksum(strings.ToLower("0x5D48aF8Ae8708d327d39481B0F397Eb260A069c4"))
	assert.Equal(t, "0x5D48aF8Ae8708d327d39481B0F397Eb260A069c4", result)
}

func TestGetAddressWithout0x(t *testing.T) {
	addr, err := eth.GetAddressWithout0x("0x9258da398C416f5e6434C6Ce2b5164657a26a6eC")
	assert.Equal(t, "9258da398C416f5e6434C6Ce2b5164657a26a6eC", addr)
	assert.Equal(t, err, nil)
}

func TestGetAddressWithout0xError(t *testing.T) {
	_, err := eth.GetAddressWithout0x("0x9258da398C416f5e6434C6Ce2b")
	assert.Errorf(t, err, "It should throw an error, the address is not correct.")

	_, err = eth.GetAddressWithout0x("0xGG58da398C416f5e6434C6Ce2b5164657a26a6GG")
	assert.Errorf(t, err, "It should throw an error, the address is not correct.")
}
