package test

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/vinicius1889/godash/geolocation"
)

func TestDistance(t *testing.T) {
	lat1 := 32.9697
	long1 := -96.80322
	lat2 := 29.46786
	long2 := -98.53506

	d := geolocation.Distance(lat1, long1, lat2, long2)
	assert.Equal(t, math.Trunc(d), float64(422))
}
