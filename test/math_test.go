package test

import (
	"testing"

	"gitlab.com/vinicius1889/godash/math"

	"gopkg.in/go-playground/assert.v1"
)

func TestSum(t *testing.T) {

	values := make([]interface{}, 3)
	values[0] = 1515.3
	values[1] = 1000
	values[2] = "1000"

	result := math.Sum(values)
	assert.Equal(t, 3515.3, result)
}

func TestMax(t *testing.T) {

	values := make([]interface{}, 3)
	values[0] = 1515.3
	values[1] = 1000
	values[2] = "1000"

	result := math.Max(values)
	assert.Equal(t, 1515.3, result)
}

func TestMean(t *testing.T) {

	values := make([]interface{}, 3)
	values[0] = 1000.0
	values[1] = 1000
	values[2] = "1000"

	result := math.Mean(values)
	assert.Equal(t, 1000.0, result)
}

func TestMin(t *testing.T) {

	values := make([]interface{}, 3)
	values[0] = -50.5
	values[1] = 152536
	values[2] = "-1000"

	result := math.Min(values)
	assert.Equal(t, -1000.0, result)
}
