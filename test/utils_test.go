package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/vinicius1889/godash/utils"
)

/*
	<DOC>
	## Utils package

	To use this package, you should import first:

	<PRE>
	```
	import (
		"testing"

		"gitlab.com/vinicius1889/godash/utils"
		"github.com/stretchr/testify/assert"
	)
	```
	</PRE>
	</DOC>
*/

func TestAttempt(t *testing.T) {
	function1 := func(a ...utils.Any) interface{} {
		panic("We have a problem")
	}
	result := utils.Attempt(function1, 0)
	assert.Equal(t, result, 0)

	function2 := func(a ...utils.Any) interface{} {
		return 100
	}
	result = utils.Attempt(function2, 0)
	assert.Equal(t, result, 100)
}
func TestDefaultTo(t *testing.T) {
	result := utils.DefaultTo(0, 1000)
	assert.Equal(t, result, 1000)

	result = utils.DefaultTo(1, 1000)
	assert.Equal(t, result, 1)

	result = utils.DefaultTo("", "me")
	assert.Equal(t, result, "me")

	user := make(map[string]interface{})
	user["name"] = "default user"

	result = utils.DefaultTo(nil, user)
	assert.Equal(t, result, user)

}

func TestFlow(t *testing.T) {
	a := func(nums ...interface{}) interface{} {
		return nums[0].(int) + nums[1].(int)
	}
	b := func(num interface{}) interface{} {
		return num.(int) * 10
	}
	c := func(num interface{}) interface{} {
		return num.(int) * 100
	}

	flow := utils.Flow(a, b, c)

	assert.Equal(t, flow(1, 2), c(b(a(1, 2))))
}

func TestTimes(t *testing.T) {
	/*
		<DOC>
		### This is a function here
	*/
	// <CODE>
	// <PRE>
	count := 0
	a := func() {
		count += 1
	}
	utils.Times(10, a)
	assert.Equal(t, 10, count)
	// </PRE>
	// </CODE>

	// </DOC>
}
