## Utils package

To use this package, you should import first:

	```
	import (
		"testing"

		"gitlab.com/vinicius1889/godash/utils"
		"github.com/stretchr/testify/assert"
	)
	```
### This is a function here

```
	count := 0
	a := func() {
		count += 1
	}
	utils.Times(10, a)
	assert.Equal(t, 10, count)

```

