package utils

import (
	"math/rand"
	"time"
)

func Rand() *rand.Rand {
	temp := rand.NewSource(time.Now().Unix() + int64(rand.Intn(777777)))
	return rand.New(temp)
}

type Any interface{}

func Attempt(function func(any ...Any) interface{}, rollback interface{}, any ...Any) interface{} {
	var result interface{}
	func() {
		defer func() {
			r := recover()
			if r != nil {
				result = rollback
			}
		}()
		result = function(any)
	}()
	return result
}

func DefaultTo(value interface{}, defaultValue interface{}) interface{} {
	if value == nil || value == "" || value == 0 {
		return defaultValue
	}
	return value
}

func Flow(initial func(i ...interface{}) interface{}, functions ...func(i interface{}) interface{}) func(i ...interface{}) interface{} {

	function := func(items ...interface{}) interface{} {
		var result interface{}
		result = initial(items...)
		for _, functionToCall := range functions {
			result = functionToCall(result)
		}
		return result
	}

	return function
}

func Times(num int, function func()) {
	for i := 0; i < num; i++ {
		function()
	}
}
